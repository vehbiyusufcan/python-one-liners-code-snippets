import math
import re
from icecream import ic

# Multiple Variable Assignments
def multiple_variable_assignmnets():
    a,b,c = 4,5.5,'Hello'
    ic(a,b,c)

    k,l,*m = [1,2,3,4,5]
    ic(k,l,m)

#Sum of Even Numbers In a List
def sum_of_even_numbers_in_a_list():
    list1 = [1,2,3,4,5,6]
    sum_even = sum([num for num in list1 if num%2 == 0])
    ic(sum_even)

#Reading Files
def reading_files():
    lst = [line.strip() for line in open('README.md')]
    ic(lst)


#Writing data to file
def writing_data_to_file():
    with open("README.md",'a',newline='\n') as f: f.write("Python is awsome")

#Creating Lists
def creating_lists():
    lst = [i for i in range(0,10)]
    ic(lst)

#Mapping Lists or TypeCasting Whole List
def mapping_lists_or_type_casting_whole_list():
    list1 = ['1','2','3']
    string_to_int = list(map(int,list1))
    int_to_float = list(map(float,list1))
    ic(string_to_int)
    ic(int_to_float)

#Set Creation

def set_creation():
    creating_set = {x**2 for x in range(10) if x%2 == 0}
    ic(creating_set)

#Palindrome
def palindrome():
    text = 'level'
    if text == text[::1]:
        return True

#Space Separated integers to a List
def space_sperated_integers_to_a_list():
    list1 = list(map(int,input().split()))
    ic(list1)

#Lambda
def lambda_function():
    sqr = lambda x: x *x 
    ic(sqr(10))

#To Check The Existence of a number in a list
def to_check_the_existence_of_a_number_in_a_list():
    number = 5 
    if number in [1,2,3,4,5]:
        ic('present')

#Printing Patterns
def printing_patterns():
    n = 5
    print('\n'.join('😀' * i for i in range(1, n + 1)))

#Factorial
def factorial():
    n = 6
    ic(math.factorial(n))

#Finding Max Number
def finding_max_number():
    findmax = lambda x,y: x if x > y else y
    ic(findmax(5,14))

    #2. Method
    ic(max(100,101))

#Linear Algebra
def scale(lst, x):
    return [i*x for i in lst]

#Transpose of a matrix
def transpose_of_a_matrix():
    a=[[1,2,3],
    [4,5,6],
    [7,8,9]] 

    transpose = [list(i) for i in zip(*a)] 
    ic(transpose)

#Counting occurrence of a pattern
def counting_occurence_of_a_pattern():
    text = 'python is a programming language. python is python.'
    count = len(re.findall('python',text))
    ic(count)

#Replacing a text with some other text
def replace():
    ic("python is a programming language.python is python".replace("python",'Java'))

if __name__ == "__main__":
    multiple_variable_assignmnets()
    sum_of_even_numbers_in_a_list()
    reading_files()
    #writing_data_to_file()
    creating_lists()
    mapping_lists_or_type_casting_whole_list()
    set_creation()
    ic(palindrome())
    #space_sperated_integers_to_a_list()
    lambda_function()
    to_check_the_existence_of_a_number_in_a_list()
    printing_patterns()
    factorial()
    finding_max_number()
    ic(scale([2,3,4],2))
    transpose_of_a_matrix()
    counting_occurence_of_a_pattern()
    replace()